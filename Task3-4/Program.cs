﻿using System;

namespace Week_1_Task3_4
{   //  Program that creates a square with dimensions based on the users input
    class SquareCreator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, we are now going to create a square from your desired dimensions");
            Console.WriteLine("The square will be created from the character #");
            Console.WriteLine("Please enter size of the square");
            drawSquare(Convert.ToInt32(Console.ReadLine()));
        }
        //  Creates a square
        public static void drawSquare(int size)
        {
            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    if (row == 0 || row == size - 1 || col == 0 || col == size - 1)
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
