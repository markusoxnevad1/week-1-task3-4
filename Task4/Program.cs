﻿using System;

namespace Week_1_Task3_4
{   //  Program that draws a rectangle within a rectangle, based on the users iput for length and width
    class RectangleCreateor
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, we are now going to create a rectangle from your desired dimensions");
            Console.WriteLine("The rectangle will be created from the character #, but will also contain");
            Console.WriteLine("a smaller rectangle within itself");
            Console.WriteLine("Please enter size of the length for the rectangle");
                int length = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Good, now enter the width of the rectangle");
                int width = Convert.ToInt32(Console.ReadLine());
            drawRectangle(length, width);
        }

        //  Draws a rectangle, with another rectangle inside
        public static void drawRectangle(int length, int width)
        {
            for (int row = 0; row < length; row++)
            {
                for (int col = 0; col < width; col++)
                {
                    if (row == 0 || row == length - 1 ||
                        row == 2 && col != 1 && col != width-2 ||
                        row == length - 3 && col != 1 && col != width-2 ||
                        col == 0 || col == width - 1 ||
                        col == 2 && row != 1 && row != length -2 ||
                        col == width - 3 && row != 1 && row != length -2)
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }

        }
    }
}
